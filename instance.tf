resource "aws_instance" "instance" {
  ami                         = var.ami_id
  instance_type               = var.instance_type
  associate_public_ip_address = var.associate_public_ip_address
  availability_zone           = data.aws_availability_zones.available.names[0]
  key_name                    = var.key_name
  vpc_security_group_ids      = var.vpc_security_group_ids
  user_data                   = var.user_data
  subnet_id                   = var.subnet_id
  iam_instance_profile        = var.iam_instance_profile
  tags = {
    Name = "${var.service_name}-${var.environment}-instance"
  }
}
