# AWS Instance Module

### What does it do?

This module will allow you to create an AWS EC2 Instance

### Usage


	module  "instance" {
		source                      = "git@gitlab.com:terraform-modules12/instance.git"
		environment                 = "Production"
		service_name                = "My-Service
		ami_id                      = "ami-050949f5d3aede071"
		instance_type               = "t2.nano"
		associate_public_ip_address = true
		key_name                    = "my_ssh_key"
		vpc_security_group_ids      = ["sg-xxxxxxxxxxxx", "sg-xxxxxxxxxxxx"]
		user_data                   = ""
		subnet_id                   = "subnet-xxxxxxxxxxxx"
	}

 ### Variable definitions

- environment: The environment this Instance belongs to (development/production)
- service_name: The name of the service this Instance is for (MyWebsite)
- ami_id: The id of the AMI to use when provisioning this Instance
- instance_type: The instance type to use when provisioning this Instance (t2.nano, t2.medium etc)
- associate_public_ip_address: (boolean) Whether to associate a public IP address to this Instance.
- key_name: the name of the ssh key to use when bootstrapping this Instance
- vpc_security_group_id: A list of security group ids to associate with this Instance
- user_data: A string of commands to run on the Instance during bootstrapping 
- subnet_id: The id of the subnet in which to provision this instance
