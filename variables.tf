variable "service_name" {
  description = "The name of the service this instance is running"
  type        = string
}

variable "environment" {
  description = "The environemnt this instance is running in"
  type            = string
}

variable "ami_id" {
  description = "The id of the Amazon Machine Image to use for this instance"
  type        = string
}

variable "instance_type" {
  description = "The instance type to use for this instance (t2.nano/t3.medium etc)"
  type        = string
}

variable "associate_public_ip_address" {
  description = "Whether to associate a public IP Address to this instance"
  type        = bool
}

variable "key_name" {
  description = "The SSH Key name to use to access this instance"
  type        = string
}

variable "vpc_security_group_ids" {
  description = "A list of Security Group id's to associate this Instance with"
  type        = list(any)
}

variable "user_data" {
  description = "The user data to attach to this instance during boot"
  type        = string
}

variable "subnet_id" {
  description = "The subnet to launch this instance in"
  type        = string
}

variable "iam_instance_profile" {
  description = "The iam instance profile to atach to this instance"
}
